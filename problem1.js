//    Given the dataset of individuals,
//  write a function that accesses and returns the email addresses of all individuals.

function getEmailsAddress(arrayOfObjects) {

    if (typeof (arrayOfObjects) === 'object' && (arrayOfObjects.length > 0)) {

        let size = arrayOfObjects.length;

        let email_var = [];
        for (let index = 0; index < size; index++) {
            email_var.push(arrayOfObjects[index]['email']);
        }
        return email_var;
    } else {
        return [];
    }

};


module.exports = getEmailsAddress;