// Q2 Find all users staying in Germany.

console.log("here we are doing some change to use git --amend function in our project ")

function usersLocation(usersData, inputLocation) {

    if (typeof (usersData) === 'object') {

        const keyList = Object.keys(usersData);
        let size = keyList.length;
        let result = []
        for (let index = 0; index < size; index++) {
            let key = keyList[index];

            if (usersData[key]['nationality'] === inputLocation) {
                result.push(key)
            }
        }
        return result;

    } else {
        return []
    }
}

module.exports = usersLocation