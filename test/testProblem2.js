// Q2 Find all users staying in Germany.


const users = require('../data')

const usersLocation = require('../problem2')

userList = usersLocation(users, 'Germany')

if (userList != []) {
    console.log("list of user staying in Germany ", userList)

} else {
    console.log("Check before passing data as an argument")
}